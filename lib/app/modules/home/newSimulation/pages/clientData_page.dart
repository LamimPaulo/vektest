import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:vekTest/app/modules/home/home_module.dart';
import 'package:vekTest/app/modules/home/newSimulation/pages/clientTaxes_page.dart';
import 'package:vekTest/app/modules/home/newSimulation/newSimulation_bloc.dart';

class ClientDataPage extends StatefulWidget {
  @override
  _ClientDataPageState createState() => _ClientDataPageState();
}

class _ClientDataPageState extends State<ClientDataPage> {
  var bloc = HomeModule.to.getBloc<NewSimulationBloc>();
  var _value;
  var cpfMask = new MaskTextInputFormatter(
    mask: '###.###.###-##',
    filter: {
      "#": RegExp(r'[0-9]'),
    },
  );
  var phoneMask = new MaskTextInputFormatter(
    mask: '(##) # ####-####',
    filter: {
      "#": RegExp(r'[0-9]'),
    },
  );

  @override
  void initState() {
    super.initState();
    bloc.cpf = '0';
    bloc.phone = '0';
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () => clear(context),
      child: Scaffold(
        backgroundColor: Colors.blueGrey[50],
        body: SafeArea(
          top: true,
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: 50,
                  color: Colors.blueGrey[50],
                  child: Stack(
                    children: [
                      InkWell(
                        onTap: () => clear(context),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Icon(Icons.arrow_back_ios),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            child: Row(
                              children: [
                                InkWell(
                                  onTap: () => clear(context),
                                  child: Text(
                                    'Simulação',
                                    style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 50,
                left: 0,
                right: 0,
                bottom: 0,
                child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          Text(
                            'Dados do cliente',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                              color: Colors.black,
                            ),
                          ),
                          Text(
                            'Os campos obrigatórios estão sinalizados com *',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: Colors.black.withOpacity(.5),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text('CPF *'),
                                  Container(
                                    width: 150,
                                    child: TextField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [cpfMask],
                                      onChanged: (val) => bloc.cpf = val
                                          .replaceAll('.', '')
                                          .replaceAll('-', ''),
                                      decoration: InputDecoration(
                                        isDense: true,
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text('Telefone *'),
                                  Container(
                                    width: 150,
                                    child: TextField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [phoneMask],
                                      onChanged: (val) => bloc.phone = val
                                          .trim()
                                          .replaceAll('(', '')
                                          .replaceAll(')', '')
                                          .replaceAll(' ', '')
                                          .replaceAll('-', ''),
                                      decoration: InputDecoration(
                                        isDense: true,
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text('Email'),
                                  Container(
                                    width: screenWidth - 60,
                                    child: TextField(
                                      keyboardType: TextInputType.emailAddress,
                                      onChanged: (val) =>
                                          bloc.email = val.trim(),
                                      decoration: InputDecoration(
                                        isDense: true,
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text('Ramo de atividade *'),
                                  Container(
                                    width: screenWidth - 60,
                                    child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black.withOpacity(.5),
                                          ),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: _dropDown(),
                                        )),
                                  ),
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                    )),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: 58,
                  child: FlatButton(
                    color: Colors.black.withOpacity(.75),
                    disabledColor: Colors.black.withOpacity(.25),
                    onPressed: bloc.cpf.length == 11
                        ? bloc.phone.length == 11
                            ? bloc.businessArea != null
                                ? () => [
                                      Navigator.push(
                                        context,
                                        PageTransition(
                                          type: PageTransitionType.rightToLeft,
                                          child: ClientTaxesPage(),
                                        ),
                                      ),
                                    ]
                                : null
                            : null
                        : null,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.keyboard_arrow_right,
                          size: 40,
                          color: Colors.transparent,
                        ),
                        Text(
                          'Próximo',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                        Icon(
                          Icons.keyboard_arrow_right,
                          size: 40,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _dropDown() => DropdownButton<String>(
        items: [
          DropdownMenuItem<String>(
            value: "1",
            child: Text(
              "Ramo 1",
            ),
          ),
          DropdownMenuItem<String>(
            value: "2",
            child: Text(
              "Ramo 2",
            ),
          ),
          DropdownMenuItem<String>(
            value: "3",
            child: Text(
              "Ramo 3",
            ),
          ),
        ],
        onChanged: (val) {
          setState(() {
            _value = val;
            bloc.businessArea = val.trim();
          });
        },
        hint: Text(
          "Selecione",
          style: TextStyle(
            color: Colors.black.withOpacity(.5),
          ),
        ),
        value: _value,
        underline: Container(),
        isDense: true,
        isExpanded: true,
        iconSize: 35,
        icon: Icon(Icons.keyboard_arrow_down),
      );

  clear(context) {
    Navigator.pop(context);
    bloc.cpf = '0';
    bloc.phone = '0';
    bloc.email = null;
    bloc.businessArea = null;
  }
}
