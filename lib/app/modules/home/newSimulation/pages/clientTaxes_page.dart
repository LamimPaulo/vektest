import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:vekTest/app/modules/home/home_module.dart';
import 'package:vekTest/app/modules/home/newSimulation/newSimulation_bloc.dart';
import 'package:vekTest/app/modules/home/newSimulation/pages/simulationResullt_page.dart';

class ClientTaxesPage extends StatefulWidget {
  @override
  _ClientTaxesPageState createState() => _ClientTaxesPageState();
}

class _ClientTaxesPageState extends State<ClientTaxesPage> {
  var bloc = HomeModule.to.getBloc<NewSimulationBloc>();
  var _value;
  var mask = new MaskTextInputFormatter(
      mask: '#####', filter: {"#": RegExp(r'[0-9,]')});
  var mask2 = new MaskTextInputFormatter(
      mask: '#####', filter: {"#": RegExp(r'[0-9,]')});
  var mask3 = new MaskTextInputFormatter(
      mask: '#####', filter: {"#": RegExp(r'[0-9,]')});
  var mask4 = new MaskTextInputFormatter(
      mask: '#####', filter: {"#": RegExp(r'[0-9,]')});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () => clear(context),
      child: Scaffold(
        backgroundColor: Colors.blueGrey[50],
        body: SafeArea(
          top: true,
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: 50,
                  color: Colors.blueGrey[50],
                  child: Stack(
                    children: [
                      InkWell(
                        onTap: () => clear(context),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Icon(Icons.arrow_back_ios),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            child: Row(
                              children: [
                                InkWell(
                                  onTap: () => clear(context),
                                  child: Text(
                                    'Simulador',
                                    style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 50,
                left: 0,
                right: 0,
                bottom: 0,
                child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          Text(
                            'Informações de taxas',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                              color: Colors.black,
                            ),
                          ),
                          Text(
                            'Os campos obrigatórios estão sinalizados com *',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: Colors.black.withOpacity(.5),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text('Concorrente *'),
                                  Container(
                                    width: screenWidth - 60,
                                    child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black.withOpacity(.5),
                                          ),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: _dropDown(),
                                        )),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'Débito',
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 16),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    'Taxa do concorrente *',
                                    style: TextStyle(
                                      color: Colors.black.withOpacity(.5),
                                    ),
                                  ),
                                  Container(
                                    width: 150,
                                    child: TextField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [mask],
                                      onChanged: (val) =>
                                          bloc.currentDebitTax = val,
                                      decoration: InputDecoration(
                                        isDense: true,
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    'Desconto oferecido *',
                                    style: TextStyle(
                                      color: Colors.black.withOpacity(.5),
                                    ),
                                  ),
                                  Container(
                                    width: 150,
                                    child: TextField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [mask2],
                                      onChanged: (val) =>
                                          bloc.ourDebitTax = val,
                                      decoration: InputDecoration(
                                        isDense: true,
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'Crédito',
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 16),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    'Taxa do concorrente *',
                                    style: TextStyle(
                                      color: Colors.black.withOpacity(.5),
                                    ),
                                  ),
                                  Container(
                                    width: 150,
                                    child: TextField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [mask3],
                                      onChanged: (val) =>
                                          bloc.currentCreditTax = val,
                                      decoration: InputDecoration(
                                        isDense: true,
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    'Desconto oferecido *',
                                    style: TextStyle(
                                      color: Colors.black.withOpacity(.5),
                                    ),
                                  ),
                                  Container(
                                    width: 150,
                                    child: TextField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [mask4],
                                      onChanged: (val) =>
                                          bloc.ourCreditTax = val,
                                      decoration: InputDecoration(
                                        isDense: true,
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    )),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: 58,
                  child: FlatButton(
                    color: Colors.black.withOpacity(.75),
                    disabledColor: Colors.black.withOpacity(.25),
                    onPressed: bloc.currentCompetitor != null
                        ? bloc.currentCreditTax != null
                            ? bloc.currentDebitTax != null
                                ? bloc.ourCreditTax != null
                                    ? bloc.ourDebitTax != null
                                        ? () => [
                                              Navigator.push(
                                                context,
                                                PageTransition(
                                                  type: PageTransitionType
                                                      .rightToLeft,
                                                  child: SimulationResultPage(),
                                                ),
                                              ),
                                            ]
                                        : null
                                    : null
                                : null
                            : null
                        : null,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.keyboard_arrow_right,
                          size: 40,
                          color: Colors.transparent,
                        ),
                        Text(
                          'Simular',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                        Icon(
                          Icons.keyboard_arrow_right,
                          size: 40,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _dropDown() => DropdownButton<String>(
        items: [
          DropdownMenuItem<String>(
            value: "1",
            child: Text(
              "Concorrente 1",
            ),
          ),
          DropdownMenuItem<String>(
            value: "2",
            child: Text(
              "Concorrente 2",
            ),
          ),
          DropdownMenuItem<String>(
            value: "3",
            child: Text(
              "Concorrente 3",
            ),
          ),
        ],
        onChanged: (val) {
          setState(() {
            _value = val;
            bloc.currentCompetitor = val;
          });
        },
        hint: Text(
          "Selecione",
          style: TextStyle(
            color: Colors.black.withOpacity(.5),
          ),
        ),
        underline: Container(),
        value: _value,
        isDense: true,
        isExpanded: true,
        iconSize: 35,
        icon: Icon(Icons.keyboard_arrow_down),
      );

  clear(context) {
    Navigator.pop(context);
    bloc.currentCompetitor = null;
    bloc.currentCreditTax = null;
    bloc.ourCreditTax = null;
    bloc.currentDebitTax = null;
    bloc.ourDebitTax = null;
  }
}
