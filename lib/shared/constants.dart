const BASE_URL = "https://api.vek.com/"; // prod
// const BASE_URL = "http://10.0.2.2:8000/api"; // android emulator
// const BASE_URL = "http://127.0.0.1:8000/api"; // device/ios emulator
//
// command => adb reverse tcp:8000 tcp:8000
// adb reverse --list | on linux to forward adb connected calls to computer local host.
