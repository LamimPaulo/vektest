import 'package:dio/dio.dart';

class CustomIntercetors extends InterceptorsWrapper {
  @override
  onRequest(RequestOptions options) async {
    return options;
  }

  @override
  onResponse(Response response) {
    //200
    //201
    print("RESPONSE[${response.statusCode}] => PATH: ${response.request.path}");
    throw response;
  }

  @override
  onError(DioError e) async {
    //Exception
    print("ERROR[${e.response.statusCode}] => PATH: ${e.request.path}");

    return e;
  }
}
