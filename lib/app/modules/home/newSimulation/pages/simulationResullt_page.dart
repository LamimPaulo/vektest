import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:vekTest/app/modules/home/home_module.dart';
import 'package:vekTest/app/modules/home/newSimulation/newSimulation_bloc.dart';

class SimulationResultPage extends StatefulWidget {
  @override
  _SimulationResultPageState createState() => _SimulationResultPageState();
}

class _SimulationResultPageState extends State<SimulationResultPage> {
  var bloc = HomeModule.to.getBloc<NewSimulationBloc>();
  var _value;
  double maxDebit = 2.0;
  double maxCredit = 3.0;
  bool allowedDebit;
  bool allowedCredit;
  bool allowedTaxes;
  TextStyle _headers = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w400,
  );

  @override
  void initState() {
    super.initState();
    checkTaxes();
  }

  checkTaxes() {
    maxDebit <=
            double.parse(
              bloc.ourDebitTax.replaceAll(',', '.'),
            )
        ? setState(() {
            allowedDebit = true;
          })
        : setState(() {
            allowedDebit = false;
          });
    maxCredit <=
            double.parse(
              bloc.ourCreditTax.replaceAll(',', '.'),
            )
        ? setState(() {
            allowedCredit = true;
          })
        : setState(() {
            allowedCredit = false;
          });

    allowedDebit && allowedCredit
        ? setState(() {
            allowedTaxes = true;
          })
        : setState(() {
            allowedTaxes = false;
          });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => clear(context),
      child: Scaffold(
        backgroundColor: Colors.blueGrey[50],
        body: SafeArea(
          top: true,
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: 50,
                  color: Colors.blueGrey[50],
                  child: Stack(
                    children: [
                      InkWell(
                        onTap: () => clear(context),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Icon(Icons.arrow_back_ios),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            child: Row(
                              children: [
                                InkWell(
                                  onTap: () => clear(context),
                                  child: Text(
                                    'Simulador',
                                    style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 50,
                left: 0,
                right: 0,
                bottom: 0,
                child: Container(
                  color: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 20),
                        Text(
                          allowedTaxes
                              ? 'Informações de taxas permitidas'
                              : 'Informações de taxas não permitidas',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w400,
                            color: Colors.black,
                          ),
                        ),
                        Text(
                          allowedTaxes
                              ? 'Todas as taxas simuladas são permitidas.\nOfereça ao seu cliente.'
                              : 'A(s) taxa(s) simulada(s) não foi(ram) permitida(s) pois não atinge(m) o mínimo requisitado.',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Colors.black.withOpacity(.5),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Table(
                          defaultColumnWidth: FlexColumnWidth(1),
                          children: [
                            TableRow(
                                decoration: BoxDecoration(
                                  border: Border(
                                    bottom: BorderSide(
                                      color: Colors.black.withOpacity(.5),
                                    ),
                                  ),
                                ),
                                children: [
                                  TableCell(
                                      child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Text(
                                      'Tipo',
                                      style: _headers,
                                    ),
                                  )),
                                  TableCell(
                                      child: Container(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10),
                                          child: Text('Concorrente',
                                              style: _headers))),
                                  TableCell(
                                      child: Container(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10),
                                          child: Text('Proposta',
                                              style: _headers))),
                                ]),
                            TableRow(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: Colors.black.withOpacity(.5),
                                  ),
                                ),
                              ),
                              children: [
                                TableCell(
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Text(
                                      'Débito',
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Text(
                                      '${double.parse(bloc.currentDebitTax.replaceAll(',', '.')).toString().replaceAll('.', ',')}%',
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 10,
                                    ),
                                    child: Text(
                                      '${double.parse(bloc.ourDebitTax.replaceAll(',', '.')).toString().replaceAll('.', ',')}%',
                                      style: TextStyle(
                                        color: allowedDebit == true
                                            ? Colors.green
                                            : Colors.red,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            TableRow(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: Colors.black.withOpacity(.5),
                                  ),
                                ),
                              ),
                              children: [
                                TableCell(
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Text(
                                      'Crédito',
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Text(
                                      '${double.parse(bloc.currentCreditTax.replaceAll(',', '.')).toString().replaceAll('.', ',')}%',
                                    ),
                                  ),
                                ),
                                TableCell(
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Text(
                                      '${double.parse(bloc.ourCreditTax.replaceAll(',', '.')).toString().replaceAll('.', ',')}%',
                                      style: TextStyle(
                                        color: allowedCredit == true
                                            ? Colors.green
                                            : Colors.red,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: 25),
                        allowedTaxes
                            ? Container()
                            : Text(
                                'O valor de taxa minima para débito é ${maxDebit.toString().replaceAll('.', ',')}%, enquanto para crédito é ${maxCredit.toString().replaceAll('.', ',')}%',
                                style: TextStyle(
                                  color: Colors.black.withOpacity(.5),
                                ),
                              ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Column(
                  children: [
                    Container(
                      height: 58,
                      child: FlatButton(
                        color: Colors.black.withOpacity(.75),
                        disabledColor: Colors.black.withOpacity(.25),
                        onPressed: () => allowedTaxes
                            ? bloc
                                .simulationAccepted(
                                  bloc.email,
                                  bloc.phone,
                                  bloc.ourDebitTax,
                                  bloc.ourCreditTax,
                                )
                                .then(
                                  (res) => res['status'] == 200
                                      ? dialog(context)
                                      : print('handle error'),
                                )
                            : Navigator.pop(context),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              allowedTaxes ? 'Proposta aceita' : 'Reajustar',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    !allowedTaxes
                        ? Container()
                        : Container(
                            height: 50,
                            child: FlatButton(
                              color: Colors.white,
                              onPressed: () => clear(context),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Recusar',
                                    style: TextStyle(
                                      color: Colors.black.withOpacity(.75),
                                      fontSize: 18,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _dropDown() => DropdownButton<String>(
        items: [
          DropdownMenuItem<String>(
            value: "1",
            child: Text(
              "Concorrente 1",
            ),
          ),
          DropdownMenuItem<String>(
            value: "2",
            child: Text(
              "Concorrente 2",
            ),
          ),
          DropdownMenuItem<String>(
            value: "3",
            child: Text(
              "Concorrente 3",
            ),
          ),
        ],
        onChanged: (val) {
          setState(() {
            _value = val;
            bloc.currentCompetitor = val;
          });
        },
        hint: Text(
          "Selecione",
          style: TextStyle(
            color: Colors.black.withOpacity(.5),
          ),
        ),
        underline: Container(),
        value: _value,
        isDense: true,
        isExpanded: true,
        iconSize: 35,
        icon: Icon(Icons.keyboard_arrow_down),
      );

  dialog(context) {
    AwesomeDialog(
      dismissOnBackKeyPress: false,
      dismissOnTouchOutside: false,
      context: context,
      animType: AnimType.SCALE,
      dialogType: DialogType.NO_HEADER,
      body: Column(
        children: [
          Text(
            'Aceite do cliente',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Resposta gravada no banco de dados!',
            style: TextStyle(
              color: Colors.black.withOpacity(.5),
            ),
          ),
          SizedBox(
            height: 40,
          ),
          Container(
            height: 40,
            color: Colors.black.withOpacity(.75),
            child: FlatButton(
              onPressed: () =>
                  Navigator.of(context).popUntil((route) => route.isFirst),
              child: Text(
                'Voltar para o inicio',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    )..show();
  }

  clear(context) {
    Navigator.pop(context);
  }
}
