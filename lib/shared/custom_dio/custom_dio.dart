import 'package:dio/dio.dart';
import 'package:vekTest/shared/constants.dart';
import 'package:vekTest/shared/custom_dio/interceptors.dart';

class CustomDio {
  final Dio client;

  CustomDio(this.client) {
    client.options.baseUrl = BASE_URL;
    client.interceptors.add(CustomIntercetors());
    client.options.headers["Accept"] = "application/json";
    // client.options.connectTimeout = 3000;
  }
}
