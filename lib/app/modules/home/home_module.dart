import 'package:vekTest/app/app_module.dart';
import 'package:vekTest/app/modules/home/home_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:vekTest/app/modules/home/home_page.dart';
import 'package:vekTest/app/modules/home/newSimulation/newSimulation_bloc.dart';
import 'package:vekTest/app/modules/home/newSimulation/newSimulation_repository.dart';
import 'package:vekTest/shared/custom_dio/custom_dio.dart';

class HomeModule extends ModuleWidget {
  @override
  List<Bloc> get blocs => [
        Bloc(
          (i) => HomeBloc(),
        ),
        Bloc(
          (i) => NewSimulationBloc(
            HomeModule.to.getDependency<NewSimulationRepository>(),
          ),
        ),
      ];

  @override
  List<Dependency> get dependencies => [
        Dependency(
          (i) => NewSimulationRepository(
            AppModule.to.getDependency<CustomDio>(),
          ),
        ),
      ];

  @override
  Widget get view => HomePage();

  static Inject get to => Inject<HomeModule>.of();
}
