import 'package:dio/dio.dart';
import 'package:vekTest/shared/custom_dio/custom_dio.dart';

class NewSimulationRepository {
  final CustomDio dio;

  NewSimulationRepository(this.dio);

  Future<dynamic> simulationAccepted(Map<String, dynamic> data) async {
    try {
      // var response = await dio.client.post("/mobile/core_register", data: data);
      // return ({
      //   'status': response.statusCode,
      //   'message': response.data,
      //   'raw': response,
      // });
      return ({
        'status': 200,
        'message': 'Proposta aceita com sucesso',
        'raw': 'Raw api response',
      });
    } on DioError catch (e) {
      //
      // 200 pois sempre vai cair no dioError, já que a call não existe
      //
      // return ({
      //   'status': e.response.statusCode,
      //   'message': e.response.data['message'],
      //   'raw': e.response,
      // });
      return ({
        'status': 200,
        'message': 'Proposta aceita com sucesso',
        'raw': 'Raw api response',
      });
    }
  }
}
