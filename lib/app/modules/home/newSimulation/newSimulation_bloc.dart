import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:vekTest/app/modules/home/newSimulation/newSimulation_repository.dart';

class NewSimulationBloc extends BlocBase {
  final NewSimulationRepository repo;

  NewSimulationBloc(this.repo);

  //client
  String cpf;
  String phone;
  String email;
  String businessArea;
  //taxes
  String currentCompetitor;
  String currentDebitTax;
  String ourDebitTax;
  String currentCreditTax;
  String ourCreditTax;

  Future<dynamic> simulationAccepted(
    clientEmail,
    clientPhone,
    debitTax,
    creditTax,
  ) async {
    try {
      var response = await repo.simulationAccepted({
        "email": clientEmail,
        "phone": clientPhone,
        "debitTax": debitTax,
        "creditTax": creditTax,
      });
      return response;
    } catch (e) {
      return e;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
